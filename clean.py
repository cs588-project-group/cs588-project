#!/usr/bin/env python3

"""
  CS 488/588 Project
  Alexis, Dalton, Ebele

  clean.py

  Removes any generated files that can be rebuilt.
"""

import os
import glob


# Unix pattern-matched strings
deletableFiles = ["*.rdb", "*.txt"]


def main():
    for l in map(glob.glob, deletableFiles):
        for f in l:
            print(f"Removing {f}")
            os.remove(f)



if __name__== "__main__":
    main()
