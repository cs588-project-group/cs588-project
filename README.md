# CS588-final-project
### Alexis Murauskas, Dalton Bohning, Ebele Esimai  

The data files are stored in **data_files.zip** to conserve storage space. Be sure to `unzip data_files.zip` before attempting to use them.

`preprocess.py` puts the data into a format suitable for redis. 

`insert.py` inserts the data into the redis server running at the default port. 

`query.py` runs the 6 queries and outputs the results.