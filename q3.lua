--3. Single-Day Station Travel Times: Find travel time for station Foster NB for 5-minute intervals for Sept 22, 2011. Report travel time in seconds.


--Get the station id
local stations = redis.call('ZRANGEBYLEX', 'location:station', '[Foster NB:', '[Foster NB:\xff')
local station_id = 0
for _, s in pairs(stations) do
    station_id = string.match(s, ":([^:]+)$")
end

--Get the length for the station
local length = redis.call('HMGET', 'station:'..station_id, 'length')[1]


local function timeInterval(i)
  local function mToH(h, m)
    while m >= 60 do
      m = m - 60
      h = h + 1
    end
    return h,m
  end

  local h_start, m_start = mToH(0, i*5)
  local h_end, m_end = mToH(h_start, m_start+5)
  local time_start = string.format("%02d", h_start)..':'..string.format("%02d", m_start)..':00'
  local time_end = string.format("%02d", h_end)..':'..string.format("%02d", m_end)..':00'
  
  return time_start, time_end
end


local result = ""
local detectors = redis.call('ZRANGEBYLEX', 'station:detector', 
                             '['..station_id..':', '['..station_id..':\xff')

for t=0,287 do --287 5-minute intervals
  local time_start, time_end = timeInterval(t)
  local date_start = '2011-09-22 '..time_start
  local date_end = '2011-09-22 '..time_end
  local sum = 0
  local total = 0

  for i, d in pairs(detectors) do
    local detector_id = string.match(d, ":(.*)")
    local speeds = redis.call('ZRANGEBYLEX', 'detector:time:loop:speed', 
                              '['..detector_id..':'..date_start, 
                              '('..detector_id..':'..date_end..'\xff')
    for _, s in pairs(speeds) do
      local speed = string.match(s, ":([^:]+)$") --Last element after ':'
      sum = sum + speed
    end
    total = total + #speeds
  end

  local avgspeed = sum/total
  local traveltime = length/avgspeed*3600

  result = result..date_start..' - '..date_end..' = '..traveltime..'\n'
end

return result






