--6. Route Finding: Find a route from Johnson Creek to Columbia Blvd on I-205 NB using the upstream and downstream fields.




--Get the initial station id
local stations = redis.call('ZRANGEBYLEX', 'location:station', 
                            '[Johnson Cr NB:', '[Johnson Cr NB:\xff')
local station_id = string.match(stations[1], ":([^:]+)$") --Last element after ":"

local result = ""
local location = ""
while location ~= "Columbia to I-205 NB" do
  local station = redis.call('ZRANGEBYLEX', 'station:downstream:location', 
                             '['..station_id..':', '['..station_id..':\xff')
  station_id = string.match(station[1], ".*:(.*):.*") --Match middle
  location = string.match(station[1], ".*:.*:(.*)")  --Match last
  result = result.."->"..location
end

return result

