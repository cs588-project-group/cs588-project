--2. Volume: Find the total volume for the station Foster NB for Sept 21, 2011.


local date_start = '2011-09-21'
local date_end = '2011-09-21'
local volume = 0

local detectors = redis.call('ZRANGEBYLEX', 'location:detector', '[Foster NB', '[Foster NB\xff')
for _, d in pairs(detectors) do
    local detector_id = string.match(d, ":([^:]+)$") --Last element after ':'

    local volumes = redis.call('ZRANGEBYLEX', 'detector:time:loop:volume', 
                               '['..detector_id..':'..date_start, 
                               '('..detector_id..':'..date_end..'\xff') --\xff is a wildcard
    for _, v in pairs(volumes) do
        volume = volume + string.match(v, ":([^:]+)$")
    end
end

return volume

