--4. Peak Period Travel Times: Find the average travel time for 7-9AM and 4-6PM on September 22, 2011 for station Foster NB. Report travel time in seconds.


--Get the station id
local stations = redis.call('ZRANGEBYLEX', 'location:station', '[Foster NB:', '[Foster NB:\xff')
local station_id = 0
for _, s in pairs(stations) do
  station_id = string.match(s, ":(.*)")
end

--Get the length for the station
local length = redis.call('HMGET', 'station:'..station_id, 'length')[1]

--Get the detectors
local detectors = redis.call('ZRANGEBYLEX', 'station:detector', '['..station_id..':', '['..station_id..':\xff')

--Returns the travel time for a single range
local function getTravelTime(date_start, date_end)
  local sum = 0
  local total = 0

  for i, d in pairs(detectors) do   
    local detector_id = string.match(d, ":(.*)")
    local speeds = redis.call('ZRANGEBYLEX', 'detector:time:loop:speed', '['..detector_id..':'..date_start, '['..detector_id..':'..date_end..'\xff')
    for _, s in pairs(speeds) do
      local speed = string.match(s, ":([^:]+)$")
      sum = sum + speed
    end
    total = total + #speeds
  end

  return sum, total
end

local sum1, total1 = getTravelTime('2011-09-22 07:00:00', '2011-09-22 09:00:00')
local sum2, total2 = getTravelTime('2011-09-22 16:00:00', '2011-09-22 18:00:00')
local sum = sum1+sum2
local total = total1+total2
local avgspeed = sum/total
local traveltime = length/avgspeed*3600

return ''..traveltime..' seconds'

