--5. Peak Period Travel Times: Find the average travel time for 7-9AM and 4-6PM on September 22, 2011 for the I-205 NB freeway. Report travel time in minutes.


--Get the highway id
local highways = redis.call('ZRANGEBYLEX', 'highwayname:shortdirection:highway', 
                            '[I-205:N', '[I-205:N\xff')
local highway_id = string.match(highways[1], ":([^:]+)$") --Last element after ":"

--Calculate the length for this highway
local length = 0
local lengths = redis.call('ZRANGEBYLEX', 'highway:station:length', 
                           '['..highway_id..':', '['..highway_id..':\xff')
for _, l in pairs(lengths) do
  length = length + string.match(l, ":([^:]+)$")
end

--Get the detectors
local detectors = redis.call('ZRANGEBYLEX', 'highway:detector', 
                             '['..highway_id..':', '['..highway_id..':\xff')


--Returns the travel time for a single range
local function getTravelTime(date_start, date_end)
  local sum = 0
  local total = 0

  for i, d in pairs(detectors) do
    local detector_id = string.match(d, ":(.*)")
    local speeds = redis.call('ZRANGEBYLEX', 'detector:time:loop:speed', '['..detector_id..':'..date_start, '['..detector_id..':'..date_end..'\xff')
    for _, s in pairs(speeds) do
      local speed = string.match(s, ":([^:]+)$")
      sum = sum + speed
    end
    total = total + #speeds
  end

  return sum, total
end

local sum1, total1 = getTravelTime('2011-09-22 07:00:00', '2011-09-22 09:00:00')
local sum2, total2 = getTravelTime('2011-09-22 16:00:00', '2011-09-22 18:00:00')
local sum = sum1+sum2
local total = total1+total2
local avgspeed = sum/total
local traveltime = length/avgspeed*60

return ''..traveltime..' minutes'