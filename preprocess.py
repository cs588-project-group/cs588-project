#!/usr/bin/env python3

"""
	CS 488/588 Project
	Alexis, Dalton, Ebele

	preprocess.py

	Processes the data .csv files into .txt files for inserting into Redis.
"""


import numpy as np
import csv
import os
import zipfile



# Null values are just set to the empty string
def formatNulls(line):
        for i in range(len(line)):
                if line[i] == "":
                        line[i] = '""'
                        
                        
def processFile(filename_in, filename_out, mapFun):
	print(f'Processing {filename_in}', flush=True)
	with open(filename_in) as data_in_csv:
		data_in = csv.reader(data_in_csv, delimiter=',', quotechar='"')
		with open(filename_out, "w+") as data_out:
			numtotal, numwritten, = 0, 0
			id = 1001
			next(data_in) #Skip header
			for line in data_in:
				numtotal += 1
				formatNulls(line)
				if mapFun(data_out, line, id):
					numwritten += 1
					id += 1
	# print(f'{str(numwritten)}/{str(numtotal)} lines processed and written to {filename_out}')


def main():
        for filename in [ "highways.csv", "freeway_stations.csv", "freeway_detectors.csv", 
                          "freeway_loopdata.csv", "freeway_loopdata_onehour.csv", 
                          "freeway_loopdata_oneday.csv"]:
                if not os.path.exists(filename):
                        if not os.path.exists("data_files.zip"):
                                print("data_files.zip not found. Exiting...")
                                exit()
                        with zipfile.ZipFile("data_files.zip", 'r') as z:
                                z.extractall(".")
                        break #We should have them all now


        def map_highways(data_out, line, id):
                data_out.write(f'HMSET highway:{line[0]} shortdirection "{line[1]}"'
                               +f' direction "{line[2]}" highwayname "{line[3]}"\n')
                data_out.write(f'ZADD highwayname:shortdirection:highway 0 "{line[3]}:{line[1]}:{line[0]}"\n')
                return True

        processFile("highways.csv", "highways.txt", map_highways)

        
        def map_stations(data_out, line, id):
                data_out.write(f'HMSET station:{line[0]} highwayid {line[1]}'
                               +f' milepost {line[2]} locationtext "{line[3]}" upstream {line[4]}'
                               +f' downstream {line[5]} stationclass {line[6]} numberlanes {line[7]}'
                               +f' latlon "{line[8]}" length {line[9]}\n')

                #Create the indices
                data_out.write(f'ZADD location:station 0 "{line[3]}:{line[0]}"\n')
                data_out.write(f'ZADD highway:station:length 0 "{line[1]}:{line[0]}:{line[9]}"\n')
                data_out.write(f'ZADD station:downstream:location 0 "{line[0]}:{line[5]}:{line[3]}"\n')
                return True
                

        processFile("freeway_stations.csv", "freeway_stations.txt", map_stations)


        def map_detectors(data_out, line, id):
                data_out.write(f'HMSET detector:{line[0]} highwayid {line[1]}'
                               +f' milepost {line[2]} locationtext "{line[3]}" detectorclass {line[4]}'
                               +f' lanenumber {line[5]} stationid {line[6]}\n')

                data_out.write(f'ZADD location:detector 0 "{line[3]}:{line[0]}"\n')
                data_out.write(f'ZADD station:detector 0 "{line[6]}:{line[0]}"\n')
                data_out.write(f'ZADD highway:detector 0 "{line[1]}:{line[0]}"\n')
                return True

        processFile("freeway_detectors.csv", "freeway_detectors.txt", map_detectors)


        def map_loopdata(data_out, line, id):
                # Exclude speeds less than 5 or null
                if line[3] == '""' or int(line[3]) < 5: return False
                data_out.write(f'HMSET loop:{id} detectorid {line[0]} starttime "{line[1]}" volume {line[2]} speed {line[3]} occupancy {line[4]}\n')
                # Create the indexes
                data_out.write(  f'ZADD loopspeed {line[3]} {id}\n'
                                 +f'ZADD detector:time:loop:volume 0 "{line[0]}:{line[1]}:{id}:{line[2]}"\n'
                                 +f'ZADD detector:time:loop:speed 0 "{line[0]}:{line[1]}:{id}:{line[3]}"\n')
                return True

        for i, o in [["freeway_loopdata_onehour.csv","freeway_loopdata_onehour.txt"],
                     ["freeway_loopdata_oneday.csv","freeway_loopdata_oneday.txt"],
                     ["freeway_loopdata.csv","freeway_loopdata.txt"]]:
                processFile(i, o, map_loopdata)


if __name__ == "__main__":
        main()
