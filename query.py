#!/usr/bin/env python3

"""
  CS 488/588 Project
  Alexis, Dalton, Ebele

  query.py

  Runs the queries to answer all of the questions.
"""


import glob
import os
import sys
import subprocess


#Each question's script is contained in q1.lua, q2.lua, ...
def runQuestion(n, text):
    print(f'{n}. {text}')
    p = subprocess.run(f'redis-cli --eval q{n}.lua', shell=True, stdout=subprocess.PIPE)
    print(p.stdout.decode())


def main():
    runQuestion(1, "Count high speeds: Find the number of speeds > 100 in the data set.")


    runQuestion(2, "Volume: Find the total volume for the station Foster NB for Sept 21, 2011.")

    
    runQuestion(3, "Single-Day Station Travel Times: Find travel time for station Foster NB for 5-minute intervals for Sept 22, 2011. Repo\
rt travel time in seconds.")


    runQuestion(4, "Peak Period Travel Times: Find the average travel time for 7-9AM and 4-6PM on September 22, 2011 for station Foster NB\
. Report travel time in seconds.")

    
    runQuestion(5, "Peak Period Travel Times: Find the average travel time for 7-9AM and 4-6PM on September 22, 2011 for the I-205 NB free\
way. Report travel time in minutes.")
    

    runQuestion(6, "Route Finding: Find a route from Johnson Creek to Columbia Blvd on I-205 NB using the upstream and downstream fields.")



if __name__== "__main__":
    main()
