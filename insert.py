#!/usr/bin/env python3

"""
  CS 488/588 Project
  Alexis, Dalton, Ebele

  insert.py

  Inserts all data into the redis server
"""


import glob
import os
import sys
import subprocess


# Allow the amount of loopdata to be specified
f_loopdata = "freeway_loopdata.txt"
if len(sys.argv) > 1:
  f_loopdata = {
    "all": "freeway_loopdata.txt",
    "hour": "freeway_loopdata_onehour.txt",
    "day": "freeway_loopdata_oneday.txt"
  }.get(sys.argv[1], sys.argv[1])
  if not os.path.exists(f_loopdata):
    print(f'File not found: {f_loopdata}')
    exit(1)


insertFiles = [ "freeway_detectors.txt", 
                "freeway_stations.txt",
                "highways.txt", 
                f_loopdata]


def main():
  for f in insertFiles:
    if not os.path.exists(f):
      print(f'File not found: {f}')
      continue

    print(f'Inserting from {f}...')
    p = subprocess.run(f'cat {f} | redis-cli --pipe', shell=True)
    print()



if __name__== "__main__":
  main()
